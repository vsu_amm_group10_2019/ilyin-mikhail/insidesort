﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InsideSort
{
    class Sort
    {
        
        public static int[] QuickSort(int[] array, DataGridView dgv)
        {
            Stack<Tuple<int, int>> stack = new Stack<Tuple<int, int>>();
            stack.Push(new Tuple<int, int>(0, array.Length - 1));
            while (stack.Count > 0)
            {
                Tuple<int, int> tuple = stack.Pop();
                if (tuple.Item2 <= tuple.Item1)
                {
                    continue;
                }
                int i = Partition(array, tuple.Item1, tuple.Item2, dgv);
                if (i - tuple.Item1 > tuple.Item2 - i)
                {
                    stack.Push(new Tuple<int, int>(tuple.Item1, i - 1));
                    stack.Push(new Tuple<int, int>(i + 1, tuple.Item2));
                }
                else 
                {
                    stack.Push(new Tuple<int, int>(i + 1, tuple.Item2));
                    stack.Push(new Tuple<int, int>(tuple.Item1, i - 1));
                }
            }
            return array;
        }
        private static int Partition(int[] array, int minIndex, int maxIndex, DataGridView dgv)
        {
            
            int tmp = minIndex - 1;
            for (int i = minIndex; i < maxIndex; i++)
            {
                if (array[i] < array[maxIndex])
                {
                    tmp++;
                    Utilitis.Swap(array, tmp, i, dgv );
                }
            }
            tmp++;
            Utilitis.Swap(array, tmp, maxIndex, dgv);
            return tmp;
        }
    }
}
