﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InsideSort
{
    public class Utilitis
    {
        public static void Swap(int[] array, int i, int j, DataGridView dgv)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            
            if (i != j)
            {
                dgv[i, 0].Style.BackColor = Color.Red;
                dgv[j, 0].Style.BackColor = Color.Red;
                ArrayToGrid(array, dgv);
                dgv.Refresh();
                System.Threading.Thread.Sleep(800);
                dgv[i, 0].Style.BackColor = Color.White;
                dgv[j, 0].Style.BackColor = Color.White;
            }

        }

        public static void GridToArray(DataGridView dgv, int[] array)
        {
            for (int i=0; i< dgv.ColumnCount; i++)
            {             
                array[i] = int.Parse(dgv[i, 0].Value.ToString());  
            }
        }


        public static void ArrayToGrid(int[] array, DataGridView dgv)
        {
            for (int i = 0; i < array.Length; i++)
            {
                try
                {
                    dgv[i, 0].Value = array[i];
                }
                catch
                {
                    MessageBox.Show("Выход за границы DataGridView!");
                }
            }
   
        }


    }
}
