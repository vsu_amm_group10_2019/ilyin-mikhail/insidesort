﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InsideSort
{
    public partial class MainForm : Form
    {
        int[] array = new int[16];

        System.Windows.Forms.DataGridView dataGridView;
        public MainForm()
        {
            InitializeComponent();
            
            //настройка
            dataGridView.ColumnCount = 16;
            dataGridView.RowCount = 1;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            Utilitis.GridToArray(dataGridView, array);
            array = Sort.QuickSort(array, dataGridView);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < dataGridView.ColumnCount; i++)
            {
                dataGridView[i, 0].Value = rnd.Next(0, 100);
            }
            buttonSort.Enabled = true;
        }

    }
}
